const express = require('express');

const app = express();

app.set('port', (process.env.PORT || 3000));
app.enable('trust proxy')

app.get('/api/whoami', (req, res) => {
  const obj = {
    ip: req.ips[0] || req.connection.remoteAddress,
    language: req.get('Accept-Language').split(',')[0],
    software: req.get('User-Agent').match(/\(([^\)]+)\)/)[1]
  }
  res.json(obj);
  res.end();
});

app.listen(app.get('port'), () => {
  console.log('Server is up and listening on port', app.get('port'));
});
